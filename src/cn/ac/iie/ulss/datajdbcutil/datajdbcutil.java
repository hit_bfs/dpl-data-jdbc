/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.ac.iie.ulss.datajdbcutil;

import cn.ac.iie.ulss.sqlexception.SqlException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import org.apache.avro.Protocol;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericArray;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.BinaryEncoder;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.io.EncoderFactory;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 *
 * @author MyPC
 */
public class datajdbcutil {

    public static Map schemaMap = new HashMap();
    private static Map<String, String> schema2Type = null;
    private static Map<String, Object> schema2Value = null;

    public static HashMap<String, String> strMapSet = new HashMap<String, String>();
    public static HashMap<String, Boolean> booMapSet = new HashMap<String, Boolean>();
    public static HashMap<String, Integer> intMapSet = new HashMap<String, Integer>();
    public static HashMap<String, Byte> byteMapSet = new HashMap<String, Byte>();
    public static HashMap<String, Double> douMapSet = new HashMap<String, Double>();
    public static HashMap<String, Float> floMapSet = new HashMap<String, Float>();
    public static HashMap<String, Long> looMapSet = new HashMap<String, Long>();
    private static Protocol protocolDocs;
    private static Schema docsSchema;
    private static GenericRecord docsRecord;
    public static GenericArray docSet;
    private static Protocol protocolDoc;
    private static Schema DocSchema;
    private static Map<String, String> docDesc = new HashMap<String, String>();
    private static Map<String, String> userDesc = new HashMap<String, String>();
    public static void init(){
        try {
            protocolDocs = Protocol.parse(new File("docs.json"));
            docsSchema = datajdbcutil.protocolDocs.getType("docs");
            docsRecord = new GenericData.Record(datajdbcutil.docsSchema);
            datajdbcutil.docsRecord.put("doc_schema_name", "bfs.blog_test");
            docSet = new GenericData.Array<ByteBuffer>(1000,datajdbcutil.docsSchema.getField("doc_set").schema());
            datajdbcutil.protocolDoc = Protocol.parse(new File("doc.json"));
            datajdbcutil.DocSchema = datajdbcutil.protocolDoc.getType("doc");
        } catch (Exception e) {

        }
    }

    public datajdbcutil() {

    }

    public static int postSend(String url, byte[] seObject) throws SqlException {
        HttpClient httpClient = new DefaultHttpClient();
        int i = 0;
        try {
            HttpPost httppost = new HttpPost("http://" + url + "/dataload/");
            InputStreamEntity reqEntity = new InputStreamEntity(new ByteArrayInputStream(seObject), -1);
            reqEntity.setContentType("binary/octet-stream");
            reqEntity.setChunked(true);
            httppost.setEntity(reqEntity);
            httppost.releaseConnection();
            httpClient.execute(httppost);
        } catch (Exception ex) {
            throw new SqlException(ex.getMessage());
        } finally {
            httpClient.getConnectionManager().shutdown();
            return 1;
        }
    }

    public static byte[] avroSingleEncoder(String[] schema, Map<String, String> schema2Type) {
        try {
            DatumWriter<GenericRecord> grWriter = new GenericDatumWriter<GenericRecord>(datajdbcutil.DocSchema);
            ByteArrayOutputStream grsos = new ByteArrayOutputStream();
            BinaryEncoder grSingleDocEncoder = new EncoderFactory().binaryEncoder(grsos, null);

            Map<String, String> docDesc = new HashMap<String, String>();
            Map<String, String> userDesc = new HashMap<String, String>();

            for (String para : schema) {
                docDesc.put(para, schema2Type.get(para));
            }

            GenericRecord singleRecord = null;
            singleRecord = new GenericData.Record(datajdbcutil.DocSchema);

            if (!strMapSet.isEmpty()) {
                singleRecord.put("string_s", strMapSet);
            }
            if (!booMapSet.isEmpty()) {
                singleRecord.put("boolean_s", booMapSet);
            }
            if (!intMapSet.isEmpty()) {
                singleRecord.put("int_s", intMapSet);
            }
            if (!byteMapSet.isEmpty()) {
                singleRecord.put("bytes_s", byteMapSet);
            }
            if (!douMapSet.isEmpty()) {
                singleRecord.put("double_s", douMapSet);
            }
            if (!floMapSet.isEmpty()) {
                singleRecord.put("float_s", floMapSet);
            }
            if (!looMapSet.isEmpty()) {
                singleRecord.put("long_s", looMapSet);
            }
            grWriter.write(singleRecord, grSingleDocEncoder);
            grSingleDocEncoder.flush();
            docSet.add(ByteBuffer.wrap(grsos.toByteArray()));
            grsos.reset();

            docsRecord.put("doc_set", docSet);
            docsRecord.put("doc_desc", docDesc);
            docsRecord.put("user_desc", userDesc);

            DatumWriter<GenericRecord> docsWriter = new GenericDatumWriter<GenericRecord>(docsSchema);
            ByteArrayOutputStream docssos = new ByteArrayOutputStream();
            BinaryEncoder docsbe = new EncoderFactory().binaryEncoder(docssos, null);
            docsWriter.write(docsRecord, docsbe);
            docsbe.flush();

            return docssos.toByteArray();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static void avroDocEncoder(String[] schema, Map<String, String> schema2Type) {
        try {

            DatumWriter<GenericRecord> grWriter = new GenericDatumWriter<GenericRecord>(DocSchema);
            ByteArrayOutputStream grsos = new ByteArrayOutputStream();
            BinaryEncoder grSingleDocEncoder = new EncoderFactory().binaryEncoder(grsos, null);
            for (String para : schema) {
                docDesc.put(para, schema2Type.get(para));
            }
            GenericRecord singleRecord = null;
            singleRecord = new GenericData.Record(DocSchema);

            if (!strMapSet.isEmpty()) {
                singleRecord.put("string_s", strMapSet);
            }
            if (!booMapSet.isEmpty()) {
                singleRecord.put("boolean_s", booMapSet);
            }
            if (!intMapSet.isEmpty()) {
                singleRecord.put("int_s", intMapSet);
            }
            if (!byteMapSet.isEmpty()) {
                singleRecord.put("bytes_s", byteMapSet);
            }
            if (!douMapSet.isEmpty()) {
                singleRecord.put("double_s", douMapSet);
            }
            if (!floMapSet.isEmpty()) {
                singleRecord.put("float_s", floMapSet);
            }
            if (!looMapSet.isEmpty()) {
                singleRecord.put("long_s", looMapSet);
            }
            grWriter.write(singleRecord, grSingleDocEncoder);
            grSingleDocEncoder.flush();
            docSet.add(ByteBuffer.wrap(grsos.toByteArray()));
            grsos.reset();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static byte[] avroDocsEncoder() {
        try {          
            docsRecord.put("doc_set", docSet);
            docsRecord.put("doc_desc", docDesc);
            docsRecord.put("user_desc", userDesc);
            DatumWriter<GenericRecord> docsWriter = new GenericDatumWriter<GenericRecord>(docsSchema);
            ByteArrayOutputStream docssos = new ByteArrayOutputStream();
            BinaryEncoder docsbe = new EncoderFactory().binaryEncoder(docssos, null);
            docsWriter.write(docsRecord, docsbe);
            docsbe.flush();
            return docssos.toByteArray();
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static Map getMapOfSchema(String sch) {

        if (schema2Type.get(sch).equals("int_s")) {
            return intMapSet;
        }
        if (schema2Type.get(sch).equals("string_s")) {
            return intMapSet;
        }
        if (schema2Type.get(sch).equals("long_s")) {
            return intMapSet;
        }
        if (schema2Type.get(sch).equals("double_s")) {
            return intMapSet;
        }
        if (schema2Type.get(sch).equals("float_s")) {
            return intMapSet;
        }
        if (schema2Type.get(sch).equals("bytes_s")) {
            return intMapSet;
        }
        return null;

    }

}
