/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.ac.iie.ulss.driver;


import java.sql.DriverPropertyInfo;
import java.sql.SQLException;
import java.util.Properties;
import cn.ac.iie.ulss.connection.Connection;
/**
 *
 * @author MyPC
 */
public class UlssDriver implements java.sql.Driver {

    static final String URL_PREFIX = "jdbc:ulsssql";

    static {
        try {
            java.sql.DriverManager.registerDriver(new UlssDriver()); //1  
        } catch (SQLException E) {
            throw new RuntimeException("Can't register driver!");
        }
    }

    public UlssDriver() throws SQLException {

    }

    @Override
    public Connection connect(String url, Properties info) throws SQLException {
        if (!acceptsURL(url)) {
            System.out.println("asadsd");
            return null;
        }
        System.out.println("111111");
        int idx = url.indexOf(':', 5); // search after "jdbc:"
        System.out.println(idx);
        System.out.println(url.substring(idx + 3));
        return new Connection((idx > 0) ? url.substring(idx + 3) : null) {};
    }

    @Override
    public boolean acceptsURL(String url) throws SQLException {
        return url.startsWith(URL_PREFIX);
    }

    @Override
    public DriverPropertyInfo[] getPropertyInfo(String url, Properties info) throws SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getMajorVersion() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getMinorVersion() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean jdbcCompliant() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
