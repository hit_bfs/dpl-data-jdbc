/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.ac.iie.ulss.driver;

import cn.ac.iie.ulss.connection.Connection;
import cn.ac.iie.ulss.sqlexception.SqlException;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author 福顺
 */
public class DriverManager {

    public static Connection getConnection(String url, String name, String pass) throws IOException, Exception, SqlException {
        Process process = Runtime.getRuntime().exec("ping "+url.split(":")[2].substring(2)); // 执行Ping命令
        DataInputStream dis = new DataInputStream(process.getInputStream());
        byte[] buffer = new byte[1024 * 1000];
        int len = dis.read(buffer);
        StringBuffer sb = new StringBuffer();
        while (len > 0) {
            sb.append(new String(buffer, 0, len));
            len = dis.read(buffer);
        }
        // 从Ping的结果中，解析出丢失率
        Pattern p = Pattern.compile("(?<=\\().*%");
        Matcher m = p.matcher(sb.toString());
        boolean flag = true; // 网络是否通
        if (m.find()) {
            String str = m.group();
            // 如果丢失率等于100%，则说明网络不通
            flag = str.equals("100%") ? false : true;
        }
        if(flag==true)
            return new Connection(url);
        else{
            throw new SqlException("can not connect to the database");
           
        }
    }

}
