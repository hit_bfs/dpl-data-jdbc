/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cn.ac.iie.ulss.datajdbc;

import cn.ac.iie.ulss.connection.*;
import cn.ac.iie.ulss.driver.DriverManager;
import cn.ac.iie.ulss.sqlexception.SqlException;

/**
 *
 * @author MyPC
 */
public class UlssDataJdbc {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ClassNotFoundException,Exception,SqlException{
        // TODO code application logic here
        String url = "jdbc:ulsssql://192.168.11.95:8010/bfs";
        String sql = "insert into blog_test(origin_blog_id,type,rel_time,text,relation,blog_id,user_id) values(?,?,?,?,?,?,?)";
        Connection conn = null;
        Class.forName("cn.ac.iie.ulss.driver.UlssDriver");
        conn = DriverManager.getConnection(url, "root", "123456");
        PreparedStatement ps = conn.prepareStatement(sql);
        for (int i = 0; i < 1001; i++) {
            ps.setString(1, "1");
            ps.setInt(2, 2);
            ps.setLong(3, 3L);
            ps.setString(4, "4");
            ps.setString(5, "5");
            ps.setString(6, "6");
            ps.setInt(7, 7);
            ps.addBatch();
        }
        System.out.println(ps.executeBatch()[0]);
        //conn.close();
    }

}
